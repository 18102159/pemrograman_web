package main

import (
	"log"
	"net/http"
	"os"
	"time"
)

func main() {
	port := os.Getenv("PORT")
	http.Handle(
		"/",
		http.StripPrefix("/", http.FileServer(http.Dir("."))))
	srv := http.Server{
		Addr:         ":" + port,
		Handler:      nil,
		ReadTimeout:  60 * time.Second,
		WriteTimeout: 60 * time.Second,
	}

	log.Println("serving http at :" + port)
	if err := srv.ListenAndServe(); err != nil {
		log.Println(err)
	}
}
